# Get a weather forecast from the [Finnish Meteorological Institute][0].

Provides forecasts from all over the world in English, Swedish and Finnish language. The forecasts are most precise for Finland and surrounding countries.

The main script gets weather data from https://widget.weatherproof.fi/android/androidwidget.php, downloads icons from https://cdn.fmi.fi/symbol-images/smartsymbol/v31/p, and optionally wind gust data from https://opendata.fmi.fi.

![Conky Weather][1]

The scripts in this repository will

- download forecast information for a given location
- format and display it according to templates

## Installation

Clone the repository or download the zip to a folder of your choice. Keep in mind that all files need to stay together.

From [framagit.org](https://framagit.org/ohnonot/fmi-weather) | [Download zip file](https://framagit.org/ohnonot/fmi-weather/-/archive/master/fmi-weather-master.zip)

From [notabug.org](https://notabug.org/ohnonot/fmi-weather) | [Download zip file](https://notabug.org/ohnonot/fmi-weather/archive/master.zip)

## Dependencies

For the main script `fmi-weather`:

- [`jshon`][9], `curl`, `sed` (for formatted output)
- `bash`
- the `coreutils` package

## For the `yad-*` scripts

- [`yad`][4] (available in most distro repositories)

## Depending on the template

- `lua` - a conky version with lua support - on Arch Linux this most likely means `conky-lua-nv` or `conky-cairo`.
  On other distros, if you're unsure, look out for a package named `conky-all`. The script itself doesn't require conky; its plain text output can be used elsewhere.
- `html` - [`yad`][4], which is able to display HTML

- Some helper scripts have their own dependencies, but they will tell you if something's missing.

## Quickstart

```
cd /path/to/fmi-weather/
./yad-location-select
```

It will store your location in a config file that is sourced by `fmi-weather`.

~~~
./fmi-weather
~~~

## Usage & Configuration

The location and additional stuff can be configured in `$XDG_CONFIG_HOME/fmi-weather/conf`. A full example config file can be found in `examples`.

Most data and files are cached and only re-downloaded when necessary.

## Other scripts in this repository

- The `get_location_id` bash script will get your location's id from geonames.org (`yad-location-select` uses it).
- The `lib/get_weather_opendata` bash script is an alternative implementation that uses only data from opendata.fmi.fi.
- The `lib/wfs_describeStoredQueries` bash script allows you to get more information about what sort of data you can
  get from opendata.fmi.fi.

All scripts have `-h` switches for help.

## If you experience problems

- It might be a network problem.
- Have a look at the `lua` or `html` templates in `templates`.
- for personal changes to the templates copy them to e.g. `*.my.lua` first, and remember to refer to them as such when calling the script, e.g.: `./fmi-weather -f my.lua`
- Remove cached files with `./fmi-weather -r` or `./fmi-weather -R`
- Conky is damn buggy, at least on Arch Linux, and has been for a long time 😠

If you found a bug please open an issue in one of the git repos.

## This is not as pretty as the weather widgets I see on r/unixporn

But it's more precise than most "weather widgets".

The reason I use this is because I want to parse a lot of weather information before I leave the house, because a national forecast is is bound to be better than one from an international website halfway around the globe, and because the Finnish Metereological Institute is not trying to sell me better weather.

I am trying as far as possible to mirror [the website's way of displaying information][10] (except for making it vertical).
It might look unwieldy, but it has more granularity and objectivity than most weather widgets out there.

------------------------

Happy customizing and don't forget to look out the window sometimes!

[0]: https://ilmatieteenlaitos.fi
[1]: https://dt.iki.fi/stuff/itl/fmi-weather.png
[2]: https://conky.cc/
[4]: https://github.com/v1cont/yad
[6]: https://notabug.org/ohnonot/conky-itl-weather/issues
[9]: https://github.com/keenerd/jshon
[10]: https://en.ilmatieteenlaitos.fi/local-weather/helsinki
