-- TODO: these should be configurable,maybe read from config file?
-- also see ./get_weather -h
width=330
indices=11
fcheight=60
offset=30
---------
basedir = debug.getinfo(1).source:match("@?(.*/)")
if basedir == nil then
    basedir = './'
end
filesdir= basedir .. 'files/'
datadir= filesdir .. 'get_weather_data/'

conky.config = {
-- Ilmatieteen laitoksen sääennuste(weather forecast)conky.

	own_window = true,
	own_window_transparent = false,
	own_window_colour = '#FAFCFE',
	own_window_type = 'normal',
	own_window_argb_visual = false,

	own_window_title = 'FMI Weather',
	own_window_class = 'conky_weather',

	double_buffer = true,-- double buffering reduces flicker

-- size of text area
	minimum_width = width, minimum_height = fcheight*indices+offset-8,
	maximum_width = width,
	alignment = 'top_middle',
    gap_y = 20,
--#############################

	update_interval = 601,

   disable_auto_reload = true, -- something about Rsvg (see lua functions) crashes conky when auto-reloading

   lua_load = filesdir .. 'script.lua',
   -- Default location: Helsinki. Get location with ./get_location_id <search string>
   lua_draw_hook_pre = 'main ' .. basedir .. 'get_weather -f lua -o forecast_height=' .. fcheight .. ',width=' .. width,
};

conky.text = '';
